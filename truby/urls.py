from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
import django.views.static
from django.views.generic import DetailView
from django.shortcuts import render_to_response
from django.template import RequestContext

from truby.service.models import Service


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^ckeditor/', include('ckeditor_uploader.urls')),
    url(r'^service/(?P<slug>[-a-zA-Z0-9_]+)/$', DetailView.as_view(queryset=Service.objects.filter(published=True)),
        name='service-detail'),
    url(r'^catalog/', include('truby.category.urls')),
    url(r'^', include('djangocms_forms.urls')),
    url(r'^', include('cms.urls')),
]


def server_error(request):
    return render_to_response('500.html', {}, context_instance=RequestContext(request))


handler500 = server_error

if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [url(r'^__debug__/', include(debug_toolbar.urls)), ] + urlpatterns

    urlpatterns += (
        url(r'^media/(?P<path>.*)$', django.views.static.serve, {
            'document_root': settings.MEDIA_ROOT,
            'show_indexes': True}),

        url(r'^static/(?P<path>.*)$', django.views.static.serve, {
            'document_root': settings.STATIC_ROOT,
            'show_indexes': True}),
    )