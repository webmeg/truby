from django.contrib import admin

from .models import Testimonial


@admin.register(Testimonial)
class TestimonialAdmin(admin.ModelAdmin):
    list_display = ('name', 'company', 'phone_no', 'stars', 'status', 'created')
    list_filter = ('status', 'created')
    list_editable = ('stars', 'status')

