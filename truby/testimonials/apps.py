from django.apps import AppConfig


class TestimonialsConfig(AppConfig):
    name = 'truby.testimonials'
    verbose_name = 'Отзывы'


default_app_config = '{module}.{app}'.format(
    module=__name__,
    app=TestimonialsConfig.__name__
)
