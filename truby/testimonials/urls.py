from django.conf.urls import url

from . import views


urlpatterns = (
    url(r'^$', views.TestimonialList.as_view(), name='testimonial-list'),
    url(r'^create/$', views.TestimonialCreate.as_view(), name='testimonial-create'),
)
