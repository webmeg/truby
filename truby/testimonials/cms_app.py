# -*- coding: utf-8 -*-
__author__ = 'Evgeniy Tatarintsev'
from django.utils.translation import ugettext_lazy as _
from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool


class TestimonialsApphook(CMSApp):
    name = _('Отзывы')
    urls = ["truby.testimonials.urls"]

apphook_pool.register(TestimonialsApphook)
