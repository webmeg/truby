from django.http import JsonResponse
from django.views.generic import ListView
from django.views.generic import CreateView

from .models import Testimonial


class TestimonialList(ListView):
    def get_queryset(self):
        return Testimonial.objects.filter(status=Testimonial.APPROVED)


class TestimonialCreate(CreateView):
    model = Testimonial
    fields = ('name', 'company', 'email', 'phone_no', 'text')

    def form_valid(self, form):
        form.save()
        return JsonResponse({'status': 'ok'})

    def form_invalid(self, form):
        return JsonResponse({'status': 'error', 'errors': form.errors})
