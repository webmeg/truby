__author__ = 'Evgeniy Tatarintsev'
from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from django.utils.translation import ugettext as _

from .models import TestimonialModel
from .models import Testimonial


class TestimonialPlugin(CMSPluginBase):
    model = TestimonialModel
    name = _('Отзывы')
    render_template = "testimonials/cms/plugins/testimonials.html"

    def render(self, context, instance, placeholder):
        testimonials = Testimonial.objects.filter(
            status=Testimonial.APPROVED
        ).order_by(instance.ordering)[:instance.count]
        context.update({'instance': instance})
        context.update({'testimonials': testimonials})
        return context


plugin_pool.register_plugin(TestimonialPlugin)
