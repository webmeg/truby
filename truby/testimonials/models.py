from django.db import models
from django.utils import timezone
from cms.models import CMSPlugin, Page


class Testimonial(models.Model):
    NEW = 0
    APPROVED = 10
    REJECTED = 20
    SPAM = 30
    STATUSES = (
        (NEW, 'Новый'),
        (APPROVED, 'Одобрен'),
        (REJECTED, 'Отклонен'),
        (SPAM, 'Спам'),
    )
    name = models.CharField('Имя', max_length=100)
    photo = models.ImageField('Фото', upload_to='testimonials', null=True, blank=True)
    company = models.CharField('Компания', max_length=100, null=True, blank=True)
    phone_no = models.CharField('Телефон', max_length=15, null=True, blank=True)
    website = models.CharField('Сайт', max_length=50, null=True, blank=True)
    text = models.TextField('Текст')
    email = models.EmailField()
    stars = models.PositiveIntegerField('Оценка', choices=([(i, str(i)) for i in range(1, 6)]))
    created = models.DateTimeField('Создан', default=timezone.now)
    updated = models.DateTimeField('Обновлен', auto_now=True)
    status = models.PositiveIntegerField('Статус', default=0, choices=STATUSES)

    class Meta:
        verbose_name = 'Отзыв'
        verbose_name_plural = 'Отзывы'
        ordering = ['-created']

    def __str__(self):
        return self.name


class TestimonialModel(CMSPlugin):
    ORDERING = (
        ('-created', 'По дате'),
        ('-stars', 'По оценке'),
    )
    title = models.CharField('Заголоков блока', max_length=100, default='Отзывы наших партнеров')
    count = models.PositiveIntegerField('Кол-во в списке', default=10)
    ordering = models.CharField('Сортировка', max_length=20, choices=ORDERING, default='-created')
    page_link = models.ForeignKey(
        Page,
        verbose_name='Страница с отзывами',
        blank=True,
        null=True,
        on_delete=models.SET_NULL
    )

    def __str__(self):
        return '%s отзывов %s' % (self.count, self.get_ordering_display().lower())

    def link(self):
        if self.page_link:
            return self.page_link.get_absolute_url()
        return None
