from django.db import models
from django.utils.translation import ugettext_lazy as _
from cms.models import CMSPlugin


class InfoPluginModel(CMSPlugin):
    info_text = models.TextField(_('Описание'))
    gost = models.CharField(_('ГОСТ'), max_length=255)
    pdf = models.FileField('pdf', upload_to='info', null=True, blank=True)
    doc = models.FileField('doc', upload_to='info', null=True, blank=True)

    def __str__(self):
        return self.gost
