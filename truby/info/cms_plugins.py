__author__ = 'Evgeniy Tatarintsev'
from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from django.utils.translation import ugettext as _

from .models import InfoPluginModel


class InfoPlugin(CMSPluginBase):
    model = InfoPluginModel
    name = _('ГОСТ')
    render_template = "gost.html"

    def render(self, context, instance, placeholder):
        context.update({'instance': instance})
        return context


plugin_pool.register_plugin(InfoPlugin)
