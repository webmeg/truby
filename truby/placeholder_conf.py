__author__ = 'Evgeniy Tatarintsev'
from django.utils.translation import ugettext_lazy as _


CMS_PLACEHOLDER_CONF = {
    'head_phone': {
        'plugins': ['TextPlugin', 'LinkPlugin'],
        'name': _('Телефон в шапке'),
    },
    'head_title': {
        'plugins': ['LinkPlugin'],
        'name': _('Название компании в шапке'),
    },
    'content': {
        'plugins': ['TextPlugin', 'PicturePlugin', 'VideoPlugin', 'LinkPlugin'],
        'name': _('Контент'),
    },
    'content_left': {
        'plugins': ['TextPlugin', 'PicturePlugin', 'VideoPlugin', 'LinkPlugin'],
        'name': _('Контент, левая колонка'),
    },
    'content_right': {
        'plugins': ['TextPlugin', 'PicturePlugin', 'VideoPlugin', 'LinkPlugin'],
        'name': _('Контент, правая колонка'),
    },
    'head_email': {
        'plugins': ['LinkPlugin'],
        'name': _('Email в шапке'),
    },
    'footer_address': {
        'plugins': ['TextPlugin'],
        'name': _('Адрес в футере'),
    },
    'slider': {
        'plugins': ['PicturePlugin'],
        'name': 'Фото производства',
    },
    'map': {
        'plugins': ['SnippetPlugin'],
        'name': 'Карта проезда',
    },
    'metrics': {
        'plugins': ['SnippetPlugin'],
        'name': 'Метрики',
    },
    'testimonials': {
        'plugins': ['TestimonialPlugin'],
        'name': 'Отзывы',
    },

}
