from django.db import models

from ckeditor_uploader.fields import RichTextUploadingField


class Category(models.Model):
    title = models.CharField('Название', max_length=150)
    slug = models.SlugField(unique=True)
    published = models.BooleanField('Опубликовано', default=True)
    icon = models.ImageField('Иконка', upload_to='category')
    position = models.PositiveIntegerField('Позиция', default=0)
    menu_title = models.CharField('Название в меню', max_length=150, null=True, blank=True)
    page_title = models.CharField('Title', max_length=150, null=True, blank=True)
    page_description = models.TextField('Description', null=True, blank=True)

    class Meta:
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'
        ordering = ['position']

    def __str__(self):
        return self.title

    def get_meta_title(self):
        return self.page_title or self.title

    def get_meta_description(self):
        return self.page_description or ''

    def get_menu_title(self):
        return self.menu_title or self.title

    @models.permalink
    def get_absolute_url(self):
        if self.products.count() == 1:
            return 'product-detail', [], {'slug': self.products.first().slug, 'category': self.slug}

        return 'category-detail', [], {'slug': self.slug}

    def get_products(self):
        return self.products.filter(published=True)


class Product(models.Model):
    title = models.CharField('Название', max_length=150)
    slug = models.SlugField(unique=True)
    category = models.ForeignKey(Category, verbose_name="Категория", related_name="products")
    published = models.BooleanField('Опубликовано', default=True)
    image = models.ImageField('Изображение', upload_to='category')
    position = models.PositiveIntegerField('Позиция', default=0)
    content = RichTextUploadingField('Контент')
    description = models.TextField('Описание')
    page_title = models.CharField('Заголовок', max_length=150, null=True, blank=True)
    page_description = models.TextField('Описание заголовка', null=True, blank=True)

    class Meta:
        verbose_name = 'Товар'
        verbose_name_plural = 'Товары'
        ordering = ['position']

    def __str__(self):
        return self.title

    def get_page_title(self):
        if self.page_title:
            return self.page_title
        else:
            return self.title

    def get_page_description(self):
        if self.page_description:
            return self.page_description
        else:
            return ""

    @models.permalink
    def get_absolute_url(self):
        return 'product-detail', [], {'slug': self.slug, 'category': self.category.slug}


class Gost(models.Model):
    name = models.CharField('Название', max_length=30)
    doc = models.FileField('Документ', upload_to='gosts')
    category = models.ForeignKey(Category, verbose_name='Категория', related_name='gosts')

    class Meta:
        verbose_name = 'Гост'
        verbose_name_plural = 'Госты'

    def __str__(self):
        return self.name
