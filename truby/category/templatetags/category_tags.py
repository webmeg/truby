from django.template import Library
from truby.category.models import Category


register = Library()


@register.assignment_tag
def category_list():
    return Category.objects.filter(published=True)
