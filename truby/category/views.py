from django.views.generic import DetailView

from .models import Product
from .models import Category


class CategoryDetail(DetailView):
    def get_queryset(self):
        return Category.objects.filter(published=True)


class ProductDetail(DetailView):
    def get_queryset(self):
        return Product.objects.filter(published=True, category__slug=self.kwargs['category'])


