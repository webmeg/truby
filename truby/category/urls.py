from django.conf.urls import url

from . import views


urlpatterns = [
    url(r'^(?P<slug>[-a-zA-Z0-9_]+)/$', views.CategoryDetail.as_view(), name='category-detail'),
    url(r'^(?P<category>[-a-zA-Z0-9_]+)/(?P<slug>[-a-zA-Z0-9_]+)/$',
        views.ProductDetail.as_view(), name='product-detail'),
]
