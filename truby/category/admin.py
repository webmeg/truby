from django.contrib import admin

from .models import Category, Product, Gost


class GostAdminInline(admin.StackedInline):
    model = Gost
    extra = 0


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ('title', 'position')
    list_editable = ('position', )
    prepopulated_fields = {'slug': ('title', )}
    inlines = (GostAdminInline, )


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = ('title', 'category', 'position', 'published')
    list_editable = ('position',)
    prepopulated_fields = {'slug': ('title',)}
    search_fields = ('title', )
    list_filter = ('category', 'published')


