$(document).ready(function () {

    $('#lightslider').lightSlider({
        gallery: true,
        item: 1,
        loop:true,
        slideMargin: 0,
        thumbItem: 9,
        addClass: "production-slider"
    });

    $('[data-fancybox]').fancybox();

    $('.review-slider').lightSlider({
        item: 2,
        slideMove: 2,
        pager: false
    });

    $('.pipe-type li').on('click', function () {
        var active = $('.pipe-type .active'),
            fieldGroup = $(this).children('a').data('field-group'),
            field = $('.calc-field');

        active.removeClass('active');
        $(this).addClass('active');

        field.each(function () {
            if ($(this).hasClass(fieldGroup)) {
                $(this).removeClass('hidden');
            }
            else {
                $(this).addClass('hidden');
            }
        })
    });

    function circlePipeMass(ro, s, d) {
        if ((2 * s > d) || (s <= 0) || (d <= 0)) {
            return ('Введены некорректные значения!');
        }
        return (Math.PI * ro * s * (d - s)).toFixed(2);
    }

    function rectPipeMass(ro, a, b, s) {
        if ((2 * s > a) || (2 * s > b) || (a <= 0) || (b <= 0) || (s <= 0)) {
            return ('Введены некорректные значения!');
        }
        return (2 * ro * s * (b + a - 2 * s)).toFixed(2);
    }

    function squarePipeMass(ro, a, s) {
        if ((2 * s > a) || (a * s <= 0) || (a <= 0) || (s <= 0)) {
            return ('Введены некорректные значения!');
        }
        return (4 * ro * s * (a - s)).toFixed(2);
    }

    function ppuCapacity(d, s, l) {
        if ((d <= 0) || (s <= 0) || (l <= 0)) {
            return ('Введены некорректные значения!')
        }
        return (Math.PI * l * s * (d + s)).toFixed(2);
    }


    $('#material').on('change', function () {
        var densityField = $('#density'),
            densityValue = $(this).val();

        densityField.val(densityValue);
    });

    $('.pipe-type a').on('click', function () {
        $('.js-calc [type="number"]').val='';
        $('.result').empty();
    });

    $('.calc-btn').on('click', function () {
        var selectedPipeType = $('.pipe-type .active a').data('field-group'),
            diameter = $('#diameter').val() / 1000,
            sideA = $('#side-a').val() / 1000,
            sideB = $('#side-b').val() / 1000,
            length = $('#length').val(),
            thickness = $('#thickness').val() / 1000,
            density = $('#density').val(),
            result = 0;

        switch (selectedPipeType) {
            case 'circle':
                result = circlePipeMass(density, thickness, diameter);
                break;
            case 'rectangle':
                result = rectPipeMass(density, sideA, sideB, thickness);
                break;
            case 'square':
                result = squarePipeMass(density, sideA, thickness);
                break;
            default:
                result = ppuCapacity(diameter, thickness, length);
        }

        $('.result').text(result);
    });

    var rateStars = $('.rate-label');
    rateStars.on("change", function () {
        var prevStars = $(this).prevAll(),
            nextStars = $(this).nextAll();
        $(this).addClass("filled");
        nextStars.each(function () {
            $(this).removeClass("filled");
        });
        prevStars.each(function () {
            $(this).addClass("filled");
        })
    })
});
