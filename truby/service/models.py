from django.db import models

from ckeditor_uploader.fields import RichTextUploadingField


class Service(models.Model):
    title = models.CharField('Название', max_length=150)
    slug = models.SlugField()
    published = models.BooleanField('Опубликовано', default=True)
    icon = models.ImageField('Иконка', upload_to='service', help_text='120x120 px')
    position = models.PositiveIntegerField('Позиция', default=0)
    text = RichTextUploadingField('Описание улуги')

    class Meta:
        verbose_name = 'Услуга'
        verbose_name_plural = 'Услуги'
        ordering = ['position']

    def __str__(self):
        return self.title

    @models.permalink
    def get_absolute_url(self):
        return 'service-detail', [], {'slug': self.slug}
