from django.template import Library
from truby.service.models import Service


register = Library()


@register.assignment_tag
def service_list(count=0):
    services = Service.objects.filter(published=True)
    if count:
        services = services[:count]
    return services
